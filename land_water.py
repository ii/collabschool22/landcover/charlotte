import numpy as np
#import matplotlib.imshow as ims
import math
import pandas as pd


#The file contain an array of classes (0-13) for each pixel of a world map
#Middle (0,0,0,0 degrees) is set to (10799,21599) with colum 21599 and row 10799 as axis of the map.

f = np.reshape(np.memmap('gl-latlong-1km-landcover.bsq'), (21600,43200))

def pixel_coordinates(lat,long):
    """ 
    Takes coordinates in grades (-90, 90) and converts
    them to pixels.
    """
    if lat > 0 and long > 0:
        lat_pix = 10799 - math.floor(lat/(90/10799))
        long_pix = 21599 + math.floor(long/(180/21600))
        return (lat_pix,long_pix)
    elif lat > 0 and long < 0:
        lat_pix = 10799 - math.floor(lat/(90/10799))
        long_pix = 21599 + math.floor(long/(180/21599))
        return (lat_pix,long_pix)
    elif lat < 0 and long > 0:
        lat_pix = 10799 - math.floor(lat/(90/10800))
        long_pix = 21599 + math.floor(long/(180/21600))
        return (lat_pix,long_pix)
    elif lat < 0 and long < 0:
        lat_pix = 10799 - math.floor(lat/(90/10800))
        long_pix = 21599 + math.floor(long/(180/21599))
        return (lat_pix,long_pix)
    
def land_or_water(file, latitude, longditude):
    """
    Takes a preprocessed file (array) and coordinates, tells
    you if it is water or land.
    """
    (lat_pix,long_pix) = pixel_coordinates(latitude,longditude)
    if file[lat_pix,long_pix] > 0:
        return print(lat_pix,long_pix,'This is land')
    else:
        return print(lat_pix,long_pix,'This is water')

# Check the bounds.    
land_or_water(f, -25, -50)